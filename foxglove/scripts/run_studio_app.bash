#!/usr/bin/env bash

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

set -e

MESSAGE_CONVERTERS_ZIP=/tmp/message_converters.foxe
FOXGLOVE_DEB_DOWNLOAD_PATH=/tmp/fg_studio.deb

# Install FoxGlove if it does not exist
if [ ! "$(command -v foxglove-studio)" ]; then
    echo "FoxGlove studio was not found locally. Downloading..."
    if [ ! -f $FOXGLOVE_DEB_DOWNLOAD_PATH ]; then
        wget -O $FOXGLOVE_DEB_DOWNLOAD_PATH \
            https://github.com/foxglove/studio/releases/download/v1.83.0/foxglove-studio-1.83.0-linux-amd64.deb
    fi
    sudo apt install $FOXGLOVE_DEB_DOWNLOAD_PATH -y
fi

# install extensions

echo "Downloading extensions..."
wget https://gitlab.com/starline/foxglove/message_converters/-/releases/permalink/latest/downloads/starline.message_converters.foxe \
    -O $MESSAGE_CONVERTERS_ZIP -q

mkdir -p ~/.foxglove-studio/extensions
unzip -qo $MESSAGE_CONVERTERS_ZIP -d ~/.foxglove-studio/extensions/message_converters

echo "Extensions have been installed."

[ "$(pgrep -c -f foxglove-studio)" -gt 0 ] && echo "FoxGlove Studio is already running" && exit 1

[ -f /.dockerenv ] && ENVIRONMENT=Docker || ENVIRONMENT=Host

if [ "$1" == "--debug" ]; then
    foxglove-studio
else
    nohup foxglove-studio >/dev/null 2>&1 &
    echo "$ENVIRONMENT: FoxGlove studio is running in background"
fi
