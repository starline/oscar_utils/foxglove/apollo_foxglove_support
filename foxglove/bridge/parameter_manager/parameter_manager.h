///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <string>
#include <vector>

#include <foxglove/websocket/websocket_notls.hpp>
#include <foxglove/websocket/websocket_server.hpp>
namespace apollo {
namespace oscar {

const char empty_str[] = "";

class ParameterManager final {
 public:
  explicit ParameterManager(
      foxglove::Server<foxglove::WebSocketNoTls>& server)
      : server_(server) {}

  ParameterManager(
      foxglove::Server<foxglove::WebSocketNoTls>& server,
      const std::string& yaml_file = empty_str,
      const std::string& urdf_file = empty_str)
      : server_(server) {
    if (!yaml_file.empty()) readFromFile(yaml_file);
    if (!urdf_file.empty()) addRobotDescription(urdf_file);
  }

  void subscriptionCallback(const std::vector<std::string>& what,
                            foxglove::ParameterSubscriptionOperation name,
                            foxglove::ConnHandle ch);

  void requestCallback(const std::vector<std::string>& what,
                       const std::optional<std::string>& name,
                       foxglove::ConnHandle ch);

  void changeCallback(const std::vector<foxglove::Parameter>& params,
                      const std::optional<std::string>& wtf,
                      foxglove::ConnHandle ch);

  void readFromFile(const std::string& yaml_file);

  void printParams();

  void addRobotDescription(const std::string& urdf_file);

  std::vector<foxglove::Parameter> getParams() { return params_; }

  // for future...
  // template <typename T>
  // void addParameter(const std::string& name, T value);

  // template <typename T>
  // void updateParameter(const std::string& name, T new_value);

 private:
  std::vector<foxglove::Parameter> params_;
  foxglove::Server<foxglove::WebSocketNoTls>& server_;
};

}  // namespace oscar
}  // namespace apollo
