///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#include <future>
#include <memory>
#include <utility>

#include "cyber/record/record_message.h"

using apollo::cyber::record::RecordMessage;

RecordMessage unpack_result(RecordMessage& x) { return x; }

RecordMessage unpack_result(std::future<RecordMessage>& x) { return x.get(); }

class ConversionResult final {
 public:
  template <typename T>
  ConversionResult(T x)
      : self_(std::make_unique<ConversionResultObject<T>>(std::move(x))) {}

  // copy ctor, move ctor, assign
  ConversionResult(ConversionResult& x) = delete;
  ConversionResult(ConversionResult&& x) = default;
  ConversionResult& operator=(ConversionResult x) {
    self_ = std::move(x.self_);
    return *this;
  }

 private:
  struct IConversionResult {
    virtual ~IConversionResult() = default;
    virtual RecordMessage unpack_result_() = 0;
  };

  template <typename T>
  struct ConversionResultObject final : IConversionResult {
    T data_;
    ConversionResultObject(T x) : data_(std::move(x)) {}
    RecordMessage unpack_result_() override;
  };

  std::unique_ptr<IConversionResult> self_;

  friend RecordMessage unpack_result(ConversionResult& x) {
    return x.self_->unpack_result_();
  }
};

template <typename T>
RecordMessage ConversionResult::ConversionResultObject<T>::unpack_result_() {
  return ::unpack_result(data_);
}
