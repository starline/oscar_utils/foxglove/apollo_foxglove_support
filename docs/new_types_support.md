# Missing message schema descriptors

To convert messages from Apollo format to Foxglove, protobuf descriptors are required. The search for descriptors is carried out in two stages:
1. Automatic search in apollo protobuf factory;
2. If the descriptor is not found, then the descriptor is searched among those manually added to the ```descriptors_map``` dictionary.

If a descriptor is not received at any stage, then the channel is skipped. In this case, a message is displayed when converting records:

```
Skipping channel {changel_name} of type {type_name}. Descriptor not found

```

In this case, you need to add the required type manually.

## Adding missing types
### 1. header file

In the file [utils.cc](../utils/utils.cc) add the path to the generated message header file. Usually the path is the same as the protobuf message description, except for the extension: ```.proto``` -> ```.pb.h```.

For example, the protobuf file **modules/oscar/monitor/proto/monitor_message.proto** should be included as follows:
```cpp
#include "modules/oscar/monitor/proto/monitor_message.pb.h"
```
### 2. pair of values [type name, descriptor]

In the file [utils.cc](../utils/utils.cc) in the static dictionary descriptors_map add a pair [**type name**, **function returning a pointer to the descriptor**].

For example, for a type ```apollo.oscar.monitor.MonitorMessage``` the path to the descriptor would be ```&apollo::oscar::monitor::MonitorMessage::descriptor``` wrapped in ```DescriptorFn``` :
```cpp
   {"apollo.oscar.monitor.MonitorMessage", DescriptorFn(&apollo::oscar::monitor::MonitorMessage::descriptor)},

```

### 3. Build dependency

In the [BUILD](../utils/BUILD) file for the utils target, add the path to the target with the C++ generated message.

```python
cc_library(
     name = "utils",
     srcs = [
         "utils.cc",
     ],
     hdrs = [
         "utils.h",
     ],
     deps = [
         ...
         "//modules/oscar/monitor/proto:monitor_message_cc_proto"
     ]
)
```

### 4. Build

Building the foxglove module:

```
cd <Apollo Root dir>
sh apollo.sh build_opt_gpu foxglove
```
