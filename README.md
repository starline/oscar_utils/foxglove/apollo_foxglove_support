# Apollo-FoxGlove bridge

We introduce an Apollo-FoxGlove bridge — a package to inspect and analyze [ApolloAuto](https://github.com/ApolloAuto/apollo) data in [FoxGlove Studio](https://foxglove.dev/).

The bridge is already integrated in [Open-Source Car (OSCAR)](https://gitlab.com/starline/oscar) project. 

This repo provides standalone code for two main entities:

 - Realtime bridge between running Cyber RT nodes and FoxGlove Studio;
 - Converter from ```.record``` files to ```.mcap``` to visualize in Foxglove Studio.

![](docs/imgs/foxglove_apollo.png)
![](docs/imgs/foxglove_plots.png)

## Features

- Automatic adaptation to topology changes (Creating / destroying nodes, channels);
- Lazy subscription to channels. Without panel that uses some topic, it will not be transfered, that reduces resources consumption and traffic usage;
- Support to send parameters from config file to FoxGlove Studio;
- Topic graph visualization on nodes/channel topology changes;
- Tool to convert Cyber .record files to .mcap files
- Conversion messages ([see more here]()):
    - ```apollo.drivers.PointCloud2``` -> ```foxglove.PointCloud```
    - ```apollo.drivers.ImageRaw``` -> ```foxglove.ComressedImage```
    - ```apollo.drivers.ImageCompressed``` -> ```foxglove.ComressedImage```

## Requirements

1. Apollo 8.0 (**installed from source**)

You can try to use it with older versions of Apollo. Main differences will be in C++ include paths and build flags in BUILD files.
## Installation

1. Copy or clone repo to ```<Apollo Root dir>/modules```
2. Copy content of ```third_party``` folder to ```<Apollo Root dir>/thid_party```
3. Add following lines in ```<Apollo Root dir>/tools/workspace.bzl```:

```python
load("//third_party/foxglove_websocket:workspace.bzl", foxglove_websocket = "repo") 
load("//third_party/mcap:workspace.bzl", mcap = "repo")

def initialize_third_party():
    """ Load third party repositories.  See above load() statements. """
    foxglove_websocket()
    mcap()

```

4. Enter Apollo container;
5. Build foxglove modules: ```sh apollo.sh build_opt_gpu modules/foxglove```.

# Usage instructions

## Apollo-Foxglove bridge

To start bridge you can use one of the following methods:
1. mainboard: ```mainboard -d modules/foxglove/bridge/dag/foxglove_bridge.dag```
2. bash script: ```/apollo/modules/foxglove/scripts/run_bridge.bash [--debug]```. By default it starts bridge on background. Flag **--debug** run on foreground instead. 

## Record to Mcap converter

Converter usage:

```
/apollo/bazel-bin/modules/oscar/foxglove/record_converter/convert_record_to_mcap  <src_path> <dest_path> [--prefix PREFIX] [--compression COMRPESSION] [--max_size MAX_SIZE] [--chunk_size CHUNK_size][--compress_images COMPRESS_IMAGE]

src_path - str - file or directory with .record files;
dest_path - str - file or directory to save .mcap files;
--prefix - str - prefix to each output file;
--compression - str - compression type [lz4]/zstd;
--max_size - int - after exceeding this limit writing will continue to another file (in MBs);
--chunk_size - int - size in bytes for eachc chunk in output files;
--compress_images - bool - whether to perform jpeg compression [true]/false. Significantly speeds up writing.
```

## Running FoxGlove studio

On Ubuntu Foxglove studio can be installed and opened with the following script:
```
cd <Apollo root dir>
modules/foxglove/scripts/run_studio_app.bash
```

Or you can manually install it via apt and add [message conversion extension](https://gitlab.com/starline/foxglove/message_converters):

```bash
sudo apt install foxglove-studio

wget https://gitlab.com/starline/foxglove/message_converters/-/releases/permalink/latest/downloads/starline.message_converters.foxe -O /tmp/message_converters.foxe -q

mkdir -p ~/.foxglove-studio/extensions
unzip -qo /tmp/message_converters.foxe -d ~/.foxglove-studio/extensions/message_converters

foxglove-studio
```

By default, Bridge opens port 8765 that can be changes in [configuration file](foxglove/bridge/config/bridge_config.pb.txt).

## Message schema conversion

![](docs/imgs/foxglove_lidars.png)


[searching for message descriptors and adding new types](docs/new_types_support.md).

Large messages such as point clouds and images are converted to the format required for visualization on panels in FoxGlove on bridge side to reduce latency.

Small message conversions are implemented as an extension for foxglove studio - [Message Converters](https://gitlab.com/starline/foxglove/message_converters).

## Using parameters and loading the Vehicle 3D model into the 3D panel

![](docs/imgs/foxglove_car_model.png)


Bridge supports reading parameters from a YAML file and sending them to FoxGlove Studio. This is most useful for loading URDF car models.

To display models, the ```/robot_description``` parameter must include a request for a URDF file. Within the URDF there may be links to other files that can be left either locally or remotely in the space. You can read more about the files in [the official FoxGlove documentation](https://foxglove.dev/docs/studio/panels/3d#add-urdf).
  
## Remarks

 - Some cyber nodes that were created to be used with FoxGlove Studio can be found in main [OSCAR repo](https://gitlab.com/starline/oscar/-/tree/master);
 - Python version (deprecated, slow, without reduced support) can be found [here](https://gitlab.com/starline/oscar/-/tree/master/modules/oscar/foxglove/python?ref_type=heads).
 - Generated FoxGlove schemas can be used in another Cyber RT modules / scripts in C++ and Python.
